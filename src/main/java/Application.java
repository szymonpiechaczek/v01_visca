import jssc.SerialPort;
import jssc.SerialPortException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {

    public static void main(String[] args) throws SerialPortException, IOException {



        String port = args[0];
        SerialPort serialPort = new SerialPort(port);
        serialPort.openPort();
        serialPort.setParams(9600, 8, 1, 0);

        boolean left = true;

        while (true) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Write command: ");
            String commands = reader.readLine();

//            byte[] byteData = left ? new byte[]{1, 6, 1, 16, 1, 1, 3} : new byte[]{1, 6, 1, 16, 1, 2, 3};
            byte[] byteData = left ? new byte[]{1, 6, 1, 16, 1, 1, 3} : new byte[]{1, 6, 4};
            left = !left;

            ViscaSerialData vCmd = new ViscaSerialData();
            vCmd.commandData = byteData;
            vCmd.sourceAdr = 0;
            vCmd.destinationAdr = 1;
            byteData = vCmd.getData();

            serialPort.writeBytes(byteData);
        }

    }

}
